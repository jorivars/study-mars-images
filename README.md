# Case study: Image compression using autoencoders

This is an end-to-end case study showing how to use an autoencoder
(a type of deep neural network) to compress a set of images.

We make use of the [Cottonwood machine learning framework](
https://gitlab.com/brohrer/cottonwood), a flexible tool for
building and experimenting with neural networks and other
machine learning methods.

Our goal is to compress images taken from cameras on the
Mars Rover Curiosity so that they can be efficiently transmitted
back to Earth.

<img src="doc/mars_autoencoder.png" width="500">

The methods we use here are explained fully in a sequence of courses
in the [End-to-End Machine Learning School](http://e2eml.school/courses),
especially
* [Course 311, Autoencoder Visualization](https://end-to-end-machine-learning.teachable.com/p/neural-network-visualization)
* [Course 312, Neural Network Framework](https://end-to-end-machine-learning.teachable.com/p/write-a-neural-network-framework)
* [Course 313, Advanced Neural Network Methods](https://end-to-end-machine-learning.teachable.com/p/advanced-neural-network-methods)
* [Course 314, Hyperparameter Optimization](https://end-to-end-machine-learning.teachable.com/p/314-neural-network-optimization/)

For a video tour of this repository, you can catch [the final lecture of Course 314](https://end-to-end-machine-learning.teachable.com/courses/314-neural-network-optimization/lectures/14030332).

## Installation

First make sure you have the packages in place that it depends on.
Installing Cottonwood will ensure that most
dependecies are met. This project also uses Pillow, an image library for
Python, and Lodgepole, a set of image processing tools I find useful.

```bash
git clone https://gitlab.com/brohrer/cottonwood.git
git clone https://gitlab.com/brohrer/lodgepole.git
python3 -m pip install -e cottonwood
python3 -m pip install -e lodgepole
python3 -m pip install Pillow --user
```

Cottonwood is a machine learning framework build for rapid experimenation
and is not guaranteed to be backward compatible. This case study was
developed under version 15. Make sure this is the version you are working from:

```bash
cd cottonwood
git checkout v15
```

To use the code for this project, clone this repository and run it locally.
You can do all this at the command line. Just a heads-up that this
repository includes a dataset of 90 images and weighs in at 31 MB.

Navigate to where you want the repository to live, then run
at the command line:

```bash
git clone https://gitlab.com/brohrer/study-mars-images.git
cd study-mars-images
python3 mars_demo.py
```
Under the `reports` directory will be directory with the timestamp
of the current run. In this directory will be a `model_parameters.txt`
describing the model, a `performance_history.png` showing the error
as the model is trained, and a sequence of `nn_viz_xxx.png` files showing
the state of the network as it is trained. The `temp` directory will
contain two directories: `compressed` and `decompressed`. These contain
transformed versions of the images as they are processed in the pipeline.
The decompressed images in particular are useful to compare against the
input images.

