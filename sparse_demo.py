import os
import sparse_compressor as imc

# Choose the directories to use
image_path = os.path.join("data", "training")
model_path = os.path.join("temp", "model.pkl")
compressed_path = os.path.join("temp", "compressed")
decompressed_path = os.path.join("temp", "decompressed")

# Make sure the directories exist
try:
    os.mkdir("temp")
except Exception:
    pass
try:
    os.mkdir(compressed_path)
except Exception:
    pass
try:
    os.mkdir(decompressed_path)
except Exception:
    pass

# Compress and save the images
autoencoder = imc.train(image_path)
imc.compress(autoencoder, image_path, compressed_path)
imc.save_model(autoencoder, model_path)

# Decompress the images
autoencoder = imc.load_model(model_path)
imc.decompress(autoencoder, compressed_path, decompressed_path)
